<?php 
require "vendor/autoload.php";

$client = new GuzzleHttp\Client;

try {
//    $response = $client->post('http://todos.dev/oauth/token', [
//        'form_params' => [
//            'client_id' => 2,
//            // The secret generated when you ran: php artisan passport:install
//            'client_secret' => 'TW1e8NYAHJcp7gsUFc18skZE5nqcXLq3afWsUKtG',
//            'grant_type' => 'password',
//            'username' => 'ju.lillo.rojas@gmail.com',
//            'password' => 'laravel',
//            'scope' => '*',
//        ]
//    ]);
//
//    // You'd typically save this payload in the session
//    $auth = json_decode( (string) $response->getBody() );
//
//    $response = $client->get('http://todos.dev/api/todos', [
//        'headers' => [
//            'Authorization' => 'Bearer '.$auth->access_token,
//        ]
//    ]);
//
//    $todos = json_decode( (string) $response->getBody() );
//
//    $todoList = "";
//    foreach ($todos as $todo) {
//        $todoList .= "<li>{$todo->task}".($todo->done ? '✅' : '')."</li>";
//    }
//
//    echo "<ul>{$todoList}</ul>";
    
    $response = $client->post('http://ams.server.dev/oauth/token', [
        'form_params' => [
            'client_id' => 2,
            // The secret generated when you ran: php artisan passport:install
            'client_secret' => 'IFed6qBokusfgh5tz55BgIxR1A3l4vzoRxsofXHD',
            'grant_type' => 'password',
            'username' => 'ju.lillo.rojas@gmail.com',
            'password' => 'laravel',
            'scope' => '*',
        ]
    ]);

    // You'd typically save this payload in the session
    $auth = json_decode( (string) $response->getBody() );

    print_r($auth);
    
    $response = $client->get('http://ams.server.dev/api/user', [
        'headers' => [
            'Authorization' => 'Bearer '.$auth->access_token,
        ]
    ]);
    
    $todos = json_decode( (string) $response->getBody() );
    
    print_r($todos);

} catch (GuzzleHttp\Exception\BadResponseException $e) {
    echo "Unable to retrieve access token.";
}